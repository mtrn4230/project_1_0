function Connect(rob_IP_address,rob_port)
global MyContext;

MyContext.String = 'Unable to connect';
set(MyContext.guihandles.lbl_Console,'string',MyContext.String);
% Open a TCP connection to the robot.
MyContext.socket = tcpip(rob_IP_address, rob_port);
set(socket, 'ReadAsyncMode', 'continuous');
fopen(socket);

% Check if the connection is valid.
if(~isequal(get(socket, 'Status'), 'open'))
    warning(['Could not open TCP connection to ', robot_IP_address, ' on port ', robot_port]);
    return;
end

% Send a sample string to the server on the robot.
fwrite(socket, 'HI_01');

% Read a line from the socket. Note the line feed appended to the message in the RADID sample code.
data = fgetl(socket);

% Print the data that we got.
fprintf(char(data));
MyContext.Connect = true;
MyContext.String = 'Connected';
set(MyContext.guihandles.lbl_Console,'string',MyContext.String);

end