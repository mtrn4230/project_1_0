classdef networkModule < handle
    
    properties
        socket;
        ipAddress;
        port;
    end
    
    methods(Access = public)
        
        %% constructor
        function obj = networkModule(aIpAddress, aPort)
            obj.ipAddress = aIpAddress;
            obj.port      = aPort;
        end
        
        %% Public functions
        function success = connect(self)
            % Open a TCP connection to the robot.
            self.openSocket();
            
            % After this send a cmd to read in all variables (joints,
            % estop, vacuum and conveyor statuses)
        end
        
        function closeConnection(self)
            fclose(self.socket);
        end
        
        % DATA TIME
        function success = moveJoint(self, jointNum, angle)
            
            %data size should be 8bit for jointNum
            %and a single (32-bit) for joint angle
            
            %initialise header variables
            status    = 0;
            cmdNum    = 2;
            subCmdNum = 0;
            terminate = 1;
            seqNum    = 0;
            dataLen   = 7;
            
            %header data
            packet = uint8(zeros(dataLen, 1));
            packet(1:2) = networkModule.serializeHeader(status, cmdNum, ... 
                                                   subCmdNum, terminate,...
                                                   seqNum, dataLen);
            
            packet(3) = uint8(jointNum); 
            %sanitise data
            angle = single(angle);
            out = typecast(angle, 'uint8');
            packet(4:dataLen) = out;
            
            receivedPacket = self.sendPacket(packet);
            return;
        end

        function success = moveAllJoints(self, angles)
            
            status    = 0;
            cmdNum    = 2;
            subCmdNum = 1;
            terminate = 1;
            seqNum    = 0;
            dataLen   = 26;
            
            packet = uint8(zeros(dataLen, 1));
            packet(1:2) = networkModule.serializeHeader(status, cmdNum, ...
                                                  subCmdNum, terminate, ...
                                                  seqNum, dataLen);
            
            %sanitise data
            angles = single(angles);
            out    = typecast(angles, 'uint8');
            packet(3:dataLen) = out;
        
            receivedPacket = self.sendPacket(packet);
            return;
        end
        
        %% Move along an axis
        function success = moveLinear(self, axis, value)
            
            status    = 0;
            cmdNum    = 2;
            subCmdNum = 2;
            seqNum    = 0;
            dataLen   = 7;
            terminate = 1;
            packet = uint8(zeros(dataLen, 1));
            packet(1:2) = networkModule.serializeHeader(status, cmdNum, ...
                                      subCmdNum, terminate, ...
                                      seqNum, dataLen);
            
            % sanitise data
            value = single(value);
            out   = typecast(value, 'uint8');
            
            packet(3) = axis;
            packet(4:dataLen) = out;
            
            receviedPacket = self.sendPacket(packet);
            return
        end
        
        function data = getRobotStatus(self)
            % initialise header variables for packing
            status    = 0;
            cmdNum    = 0;
            subCmdNum = 0;
            terminate = 1;
            seqNum    = 0;
            dataLen   = 0;
            
            header = networkModule.serializeHeader(status, cmdNum, subCmdNum,...
                                                terminate, seqNum, dataLen);
                                            
            % now send the command
            % and wait for reply
            
            packet = self.sendPacket(header);
            
            networkModule.deserializeHeader(packet(0:2));
            
        end
        
        function success = setDigitalIO(self, conRun, conDir, vacPump, vacSol)
        
            status    = 0;
            cmdNum    = 4;
            subCmdNum = 0;
            terminate = 1;
            seqNum    = 0;
            dataLen   = 3;
            
            packet = uint8(zeros(dataLen, 1));
            
            packet(1:2) = networkModule.serializeHeader(status, cmdNum, ... 
                                                subCmdNum, terminate,... 
                                                seqNum, dataLen);
            
            % convert all values to uint8
            conRun  = uint8(conRun);
            conDir  = uint8(conDir);
            vacPump = uint8(vacPump);
            vacSol  = uint8(vacSol);
            
            % add them all in
            packet(3) = bitor(packet(3), bitshift(conRun, 7));
            packet(3) = bitor(packet(3), bitshift(conDir, 6));
            packet(3) = bitor(packet(3), bitshift(vacPump, 5));
            packet(3) = bitor(packet(3), bitshift(vacSol, 4));
            
            receivedPacket = self.sendPacket(packet);
            
            return;
        end
    end
    
    
    
    %% private functions
    
    % The header is comprised as 16bits and is as follows:
    % status     => 2 bits:
    %                   0 = send
    %                   1 = reply success
    %                   2 = reply failure
    %
    % cmdNum     => 3 bits:
    %                   0 = debug
    %                   1 = startup
    %                   2 = move joints
    %                   3 = read joints
    %                   4 = set digital
    %                   5 = read IO
    % subCmdNum  => 2 bits:
    %                   Depends on the number
    % terminate  => 1 bit:
    %                   0 = not last packet in this message
    %                   1 = last packet in this message
    % seqNum     =>  2 bits:
    %                   specifies which packet this is in this
    %                   communication sequence
    % DataLength =>  6 bits:
    %                   size of data packet in this packet
    %% 
    methods(Access = private, Static = true)
        function header = serializeHeader(status, cmdNum, subCmdNum,... 
                                       terminate, seqNum, dataLen)
           
            % make sure header values are correct
            networkModule.checkHeaderValues(status, cmdNum, subCmdNum, seqNum, dataLen,...
                              terminate);
                          
            header = uint8(zeros(2,1));
            
            % STATUS
            header(1) = bitor(header(1), bitshift(status, 6));
            
            % CMD
            header(1) = bitor(header(1), bitshift(cmdNum, 3));
            
            % SUBCMD
            header(1) = bitor(header(1), bitshift(subCmdNum, 1));
            
            % TERMINATE
            header(1) = bitor(header(1), terminate);
            
            % SEQNUM
            header(2) = bitor(header(2), bitshift(seqNum, 6));
            
            % DATALEN
            header(2) = bitor(header(2), dataLen);
        
            return;
        end
        
        function deserializedHeader = deserializeHeader(header)
            
            deserializedHeader = struct("status", 0, "cmdNum", 0,... 
                                        "subCmdNum", 0, "terminate", 0,...
                                        "seqNum", 0, "dataLength", 0);
            statusMsk    = 3;
            cmdMsk       = 7;
            subCmdMsk    = 3;
            terminateMsk = 1;
            seqNumMsk    = 3;
            dataLenMsk   = 63;
            
            deserializedHeader.status = bitand(statusMsk,...
                                               bitshift(header(1), -6));
                                                       
            deserializedHeader.cmdNum = bitand(cmdMsk,...
                                               bitshift(header(1), -3));
            
            deserializedHeader.subCmdNum = bitand(subCmdMsk,...
                                                  bitshift(header(1), -1));
            
            deserializedHeader.terminate = bitand(header(1), terminateMsk);
            
            deserializedHeader.seqNum = bitand(seqNumMsk,...
                                               bitshift(header(2), -6));
            
            deserializedHeader.dataLength = bitand(dataLenMsk, header(2));
            return;
        end
        
        % check that the header values fall within appropriate bit values
        function checkHeaderValues(status, cmdNum, subCmdNum, seqNum, dataLen,...
                terminate)
           
            if status < 0 || status > 2
                throw(MException("BADSTATUS",... 
                    "Status must be between 0 & 2"));
            end
            
            if cmdNum < 0 || cmdNum > 7
                throw(MException("BADCMD",...
                    "Cmd num must be between 0 & 7"));
            end
            
            if subCmdNum < 0 || subCmdNum > 3
                throw(MException("BADSUBCMD",...
                    "Sub cmd num must be between 0 & 3"));     
            end
            
            if terminate < 0 || terminate > 1
                throw(MException("BADTERM",...
                    "Terminate must be either 0 or 1"));
            end
            
            if seqNum < 0 || seqNum > 3
                throw(MException("BADSEQ",...
                    "Sequence number must be between 0 & 3")); 
            end
            
            if dataLen < 0 || dataLen > 63
                throw(MException("BADLEN",...
                    "Data length must be between 0 & 63"));
            end
        end
    end
    
    methods(Access = private, Static = false)
        function openSocket(self)
            self.socket = tcpip(self.ipAddress, self.port);
            set(self.socket, 'ReadAsyncMode', 'continuous');
            fopen(self.socket);
            % Check if the connection is valid.
            if(~isequal(get(self.socket, 'Status'), 'open'))
                warning(['Could not open TCP connection to ', self.ipAddress, ' on port ', self.port]);
                return;
            end 
        end
        
        function receivedPacket = sendPacket(self, packet)
 
            fwrite(self.socket, packet);
            %receivedPacket = fgetl(self.socket);
        end
    end
end