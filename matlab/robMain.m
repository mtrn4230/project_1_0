function robMain()
clear all; clc; close all;


%Main file to run the Gui and create all the handles

global MyContext;

%Set debug to true if working from home, away from robot comp
%Set debug to false if working with robot computer
MyContext.offline = true;

if MyContext.offline == true
    robot_IP_address = '127.0.0.1';
    robot_port = 1025;
elseif MyContext.offline == false
    robot_IP_address = '192.168.125.1';
    robot_port = 1025;
end

Init_MyContext;
MyContext.networkModule = networkModule(robot_IP_address, robot_port);
MyContext.rob = irb120((MyContext.J).*pi/180);
%Flags
F_string_send = false;

H = robgui();
guihandles = guidata(H); 
MyContext.guihandles = guihandles;

if MyContext.offline == false
    cam1 = videoinput('winvideo',1,'RGB24_1600x1200');
    cam2 = videoinput('winvideo',2,'RGB24_1600x1200');
else
    cam1 = imread('./Images/emptytable.jpg');
    cam2 = imread('./Images/emptyconveyor.jpg');
end

while MyContext.Exit == false %while exit button has not been pressed
    
    if F_string_send == false
        c = clock;
        s_c = sprintf('[%d/%d/%d %.2d:%.2d:%.2f]',c(3),c(2),c(1),c(4),c(5),c(6));
        s = 'GUI Started';
        MyContext.String = sprintf('%s %s\n',s_c,s);
        set(guihandles.lbl_Console,'string',MyContext.String);
        F_string_send = true;
    end
    
    %update irb120 model
%     axes(MyContext.guihandles.figu);
    MyContext.irb120.plot((MyContext.J).*pi/180);
    
    %update video axis
    if MyContext.offline == false
        snap1 = getsnapshot(cam1);
        snap2 = getsnapshot(cam2);
        imshow(snap1,'Parent',MyContext.guihandles.axs_vidtab);
        imshow(snap2,'Parent',MyContext.guihandles.axs_vidcon);
    else
        snap1 =(cam1);
        snap2 =(cam2);
        imshow(snap1,'Parent',MyContext.guihandles.axs_vidtab);
        imshow(snap2,'Parent',MyContext.guihandles.axs_vidcon);
    end
    
%     pause(0.00001);
end




if MyContext.Exit == true
    if MyContext.Connect == true
        %make sure to disconnect from robot
    else
        MyContext.String = sprintf('Exiting GUI...');
        set(guihandles.lbl_Console,'string',MyContext.String);
        pause(0.5);
        close(H);
        close all;
    end
end

end

function Init_MyContext()
global MyContext;

% MyContext.guihandles = guihandles;
MyContext.Connect = false;
MyContext.Run = false;
MyContext.Exit = false;
MyContext.J = [0,0,0,0,0,0];
MyContext.L = [0,0,0];
MyContext.q = [0,0,1,0];
% MyContext.slider = [0,0,0,0,90,0];
MyContext.VacPwr = false;
MyContext.VacSol = false;
MyContext.ConRun = false;
MyContext.ConDir = false; %false towards table, true away from table
MyContext.ConStat = false;
MyContext.String = '';%L(i) = Link([theta d a alpha]) where d,a in metres and theta,alpha in radians
MyContext.procImg = [0,1];

%%Initialise IRB 120 figure
%L(i) = Link([theta d a alpha]) where d,a in metres and theta,alpha in radians
L(1) = Link([0      0.290    0       pi/2]);
L(2) = Link([0      0        0.270   0]);
L(3) = Link([0      0        0.07    -pi/2]);
L(4) = Link([0      0.302    0       pi/2]);
L(5) = Link([0      0        0       pi/2]);
L(6) = Link([0      0.137    0       0]);

%L(i).offset = offset in radians
L(1).offset = pi;
L(2).offset = pi/2;
L(3).offset = 0;
L(4).offset = 0;
L(5).offset = pi;
L(6).offset = 0;

MyContext.irb120 = SerialLink(L, 'name', 'irb120');
q = (MyContext.J).*pi/180;
T = MyContext.irb120.fkine(q);
MyContext.L = T.t;



return;
end

