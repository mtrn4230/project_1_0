function varargout = robgui(varargin)
% ROBGUI MATLAB code for robgui.fig
%      ROBGUI, by itself, creates a new ROBGUI or raises the existing
%      singleton*.
%
%      H = ROBGUI returns the handle to a new ROBGUI or the handle to
%      the existing singleton*.
%
%      ROBGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in ROBGUI.M with the given input arguments.
%
%      ROBGUI('Property','Value',...) creates a new ROBGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before robgui_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to robgui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help robgui

% Last Modified by GUIDE v2.5 25-Aug-2017 10:38:09

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @robgui_OpeningFcn, ...
                   'gui_OutputFcn',  @robgui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before robgui is made visible.
function robgui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to robgui (see VARARGIN)

% Choose default command line output for robgui
handles.output = hObject;

%% Initialise Camera Feeds
global MyContext;
if MyContext.offline == false
    cam = videoinput('winvideo',1,'RGB24_1600x1200');
%     preview(cam,MyContext.guihandles.axs_vidtab);
end

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes robgui wait for user response (see UIRESUME)
% uiwait(handles.figure1);





% --- Outputs from this function are returned to the command line.
function varargout = robgui_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pbn_connection.
function pbn_connection_Callback(hObject, eventdata, handles)
% hObject    handle to pbn_connection (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global MyContext;

text = get(hObject,'String');
% toggle handling
if strcmp(text,'Connect')
    MyContext.networkModule.connect();
    % toggle text
    set(hObject,'String','Disconnect');
else
    
    fwrite(MyContext.socket,'H_02');
    
    MyContext.Connect = false;
    
    % toggle text
    set(hObject,'String','Connect');
end


% --- Executes on button press in pbn_run.
function pbn_run_Callback(hObject, eventdata, handles)
% hObject    handle to pbn_run (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global MyContext;

text = get(hObject,'String');
% toggle handling
if strcmp(text,'Start')
    MyContext.Run = true;
    % toggle text
    set(hObject,'String','Stop');
else
    MyContext.Run = false;
    
    % toggle text
    set(hObject,'String','Start');
end


% --- Executes on button press in pbn_exit.
function pbn_exit_Callback(hObject, eventdata, handles)
% hObject    handle to pbn_exit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global MyContext;
MyContext.Exit = true;


% --- Executes on button press in pbn_vacpwr.
function pbn_vacpwr_Callback(hObject, eventdata, handles)
% hObject    handle to pbn_vacpwr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global MyContext

text = get(hObject,'String');
% toggle handling
if strcmp(text,'Turn On')
    MyContext.VacPwr = true;
    % toggle text
    set(hObject,'String','Turn Off');
else
    MyContext.VacPwr = false;
    
    % toggle text
    set(hObject,'String','Turn On');
end
%TODO: ERROR HANDLING
result = MyContext.networkModule.setDigitalIO(MyContext.ConRun,... 
                                              MyContext.ConDir,... 
                                              MyContext.VacPwr,... 
                                              MyContext.VacSol);



% --- Executes on button press in pbn_conrun.
function pbn_conrun_Callback(hObject, eventdata, handles)
% hObject    handle to pbn_conrun (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global MyContext

text = get(hObject,'String');
% toggle handling
if strcmp(text,'Turn On')
    MyContext.ConRun = true;
    % toggle text
    set(hObject,'String','Turn Off');
else
    MyContext.ConRun = false;
    
    % toggle text
    set(hObject,'String','Turn On');
end
%TODO: ERROR HANDLING
result = MyContext.networkModule.setDigitalIO(MyContext.ConRun,... 
                                              MyContext.ConDir,... 
                                              MyContext.VacPwr,... 
                                              MyContext.VacSol);



% --- Executes on button press in pbn_vacsol.
function pbn_vacsol_Callback(hObject, eventdata, handles)
% hObject    handle to pbn_vacsol (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global MyContext

text = get(hObject,'String');
% toggle handling
if strcmp(text,'Turn On')
    MyContext.VacSol = true;
    % toggle text
    set(hObject,'String','Turn Off');
else
    MyContext.VacSol = false;
    
    % toggle text
    set(hObject,'String','Turn On');
end
%TODO: ERROR HANDLING
result = MyContext.networkModule.setDigitalIO(MyContext.ConRun,... 
                                              MyContext.ConDir,... 
                                              MyContext.VacPwr,... 
                                              MyContext.VacSol);



% --- Executes on button press in pbn_condir.
function pbn_condir_Callback(hObject, eventdata, handles)
% hObject    handle to pbn_condir (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global MyContext

text = get(hObject,'String');
% toggle handling
if strcmp(text,'Forward')
    MyContext.ConDir = true;
    % toggle text
    set(hObject,'String','Backward');
else
    MyContext.ConDir = false;
    
    % toggle text
    set(hObject,'String','Forward');
end
%TODO: ERROR HANDLING
result = MyContext.networkModule.setDigitalIO(MyContext.ConRun,... 
                                              MyContext.ConDir,... 
                                              MyContext.VacPwr,... 
                                              MyContext.VacSol);



% --- Executes during object creation, after setting all properties.
function lbl_j1_val_CreateFcn(hObject, eventdata, handles)
% hObject    handle to lbl_j1_val (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
global MyContext;
set(hObject,'String',num2str(MyContext.J(1)));



% --- Executes during object creation, after setting all properties.
function lbl_j2_val_CreateFcn(hObject, eventdata, handles)
% hObject    handle to lbl_j2_val (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
global MyContext;
set(hObject,'String',num2str(MyContext.J(2)));

% --- Executes during object creation, after setting all properties.
function lbl_j3_val_CreateFcn(hObject, eventdata, handles)
% hObject    handle to lbl_j3_val (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
global MyContext;
set(hObject,'String',num2str(MyContext.J(3)));

% --- Executes during object creation, after setting all properties.
function lbl_j4_val_CreateFcn(hObject, eventdata, handles)
% hObject    handle to lbl_j4_val (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
global MyContext;
set(hObject,'String',num2str(MyContext.J(4)));

% --- Executes during object creation, after setting all properties.
function lbl_j5_val_CreateFcn(hObject, eventdata, handles)
% hObject    handle to lbl_j5_val (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
global MyContext;
set(hObject,'String',num2str(MyContext.J(5)));

% --- Executes during object creation, after setting all properties.
function lbl_j6_val_CreateFcn(hObject, eventdata, handles)
% hObject    handle to lbl_j6_val (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
global MyContext;
set(hObject,'String',num2str(MyContext.J(6)));

% --- Executes on slider movement.
function sld_j1_Callback(hObject, eventdata, handles)
% hObject    handle to sld_j1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global MyContext;

j1_val = round(get(hObject,'Value'));
set(handles.lbl_j1_val,'String',string(j1_val));

%TODO: Handle cases where message isn't sent successfully
result = MyContext.networkModule.moveJoint(1, j1_val);

MyContext.J(1) = j1_val;
%apply forwad kinematics to update the pose sliders and values
MyContext.L = 1000*fkine_irb120(MyContext.rob,(MyContext.J).*pi/180);
set(MyContext.guihandles.sld_x,'Value',MyContext.L(1));
set(MyContext.guihandles.lbl_x,'String',num2str(MyContext.L(1)));

set(MyContext.guihandles.sld_y,'Value',MyContext.L(2));
set(MyContext.guihandles.lbl_y,'String',num2str(MyContext.L(2)));

set(MyContext.guihandles.sld_z,'Value',MyContext.L(3));
set(MyContext.guihandles.lbl_z,'String',num2str(MyContext.L(3)));

% RPC call to move robot


% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function sld_j1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sld_j1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
global MyContext;
set(hObject,'Value',MyContext.J(1));



% --- Executes on slider movement.
function sld_j2_Callback(hObject, eventdata, handles)
% hObject    handle to sld_j2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
global MyContext;

j2_val = round(get(hObject,'Value'));
set(handles.lbl_j2_val,'String',string(j2_val));

%TODO: Handle cases where message isn't sent successfully
result = MyContext.networkModule.moveJoint(2, j2_val);

MyContext.J(2) = j2_val;
%apply forwad kinematics to update the pose sliders and values
MyContext.L = 1000*fkine_irb120(MyContext.rob,(MyContext.J).*pi/180);
set(MyContext.guihandles.sld_x,'Value',MyContext.L(1));
set(MyContext.guihandles.lbl_x,'String',num2str(MyContext.L(1)));

set(MyContext.guihandles.sld_y,'Value',MyContext.L(2));
set(MyContext.guihandles.lbl_y,'String',num2str(MyContext.L(2)));

set(MyContext.guihandles.sld_z,'Value',MyContext.L(3));
set(MyContext.guihandles.lbl_z,'String',num2str(MyContext.L(3)));

% --- Executes during object creation, after setting all properties.
function sld_j2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sld_j2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
global MyContext;
set(hObject,'Value',MyContext.J(2));

% --- Executes on slider movement.
function sld_j3_Callback(hObject, eventdata, handles)
% hObject    handle to sld_j3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
global MyContext;

j3_val = round(get(hObject,'Value'));
set(handles.lbl_j3_val,'String',string(j3_val));

%TODO: Handle cases where message isn't sent successfully
result = MyContext.networkModule.moveJoint(3, j3_val);

MyContext.J(3) = j3_val;
%apply forwad kinematics to update the pose sliders and values
MyContext.L = 1000*fkine_irb120(MyContext.rob,(MyContext.J).*pi/180);
set(MyContext.guihandles.sld_x,'Value',MyContext.L(1));
set(MyContext.guihandles.lbl_x,'String',num2str(MyContext.L(1)));

set(MyContext.guihandles.sld_y,'Value',MyContext.L(2));
set(MyContext.guihandles.lbl_y,'String',num2str(MyContext.L(2)));

set(MyContext.guihandles.sld_z,'Value',MyContext.L(3));
set(MyContext.guihandles.lbl_z,'String',num2str(MyContext.L(3)));

% --- Executes during object creation, after setting all properties.
function sld_j3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sld_j3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
global MyContext;
set(hObject,'Value',MyContext.J(3));

% --- Executes on slider movement.
function sld_j4_Callback(hObject, eventdata, handles)
% hObject    handle to sld_j4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
global MyContext;

j4_val = round(get(hObject,'Value'));
set(handles.lbl_j4_val,'String',string(j4_val));

%TODO: Handle cases where message isn't sent successfully
result = MyContext.networkModule.moveJoint(4, j4_val);

MyContext.J(4) = j4_val;
%apply forwad kinematics to update the pose sliders and values
MyContext.L = 1000*fkine_irb120(MyContext.rob,(MyContext.J).*pi/180);
set(MyContext.guihandles.sld_x,'Value',MyContext.L(1));
set(MyContext.guihandles.lbl_x,'String',num2str(MyContext.L(1)));

set(MyContext.guihandles.sld_y,'Value',MyContext.L(2));
set(MyContext.guihandles.lbl_y,'String',num2str(MyContext.L(2)));

set(MyContext.guihandles.sld_z,'Value',MyContext.L(3));
set(MyContext.guihandles.lbl_z,'String',num2str(MyContext.L(3)));

% --- Executes during object creation, after setting all properties.
function sld_j4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sld_j4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
global MyContext;
set(hObject,'Value',MyContext.J(4));


% --- Executes on slider movement.
function sld_j5_Callback(hObject, eventdata, handles)
% hObject    handle to sld_j5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
global MyContext;

j5_val = round(get(hObject,'Value'));
set(handles.lbl_j5_val,'String',string(j5_val));

%TODO: Handle cases where message isn't sent successfully
result = MyContext.networkModule.moveJoint(5, j5_val);

MyContext.J(5) = j5_val;
%apply forwad kinematics to update the pose sliders and values
MyContext.L = 1000*fkine_irb120(MyContext.rob,(MyContext.J).*pi/180);
set(MyContext.guihandles.sld_x,'Value',MyContext.L(1));
set(MyContext.guihandles.lbl_x,'String',num2str(MyContext.L(1)));

set(MyContext.guihandles.sld_y,'Value',MyContext.L(2));
set(MyContext.guihandles.lbl_y,'String',num2str(MyContext.L(2)));

set(MyContext.guihandles.sld_z,'Value',MyContext.L(3));
set(MyContext.guihandles.lbl_z,'String',num2str(MyContext.L(3)));

% --- Executes during object creation, after setting all properties.
function sld_j5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sld_j5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
global MyContext;
set(hObject,'Value',MyContext.J(5));
% pose = fkine_irb120(MyContext.rob,MyContext.J.*pi/180)


% --- Executes on slider movement.
function sld_j6_Callback(hObject, eventdata, handles)
% hObject    handle to sld_j6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
global MyContext;

j6_val = round(get(hObject,'Value'));
set(handles.lbl_j6_val,'String',string(j6_val));
MyContext.J(6) = j6_val;

%TODO: Handle cases where message isn't sent successfully
result = MyContext.networkModule.moveJoint(6, j6_val);

%apply forwad kinematics to update the pose sliders and values
MyContext.L = 1000*fkine_irb120(MyContext.rob,(MyContext.J).*pi/180);
set(MyContext.guihandles.sld_x,'Value',MyContext.L(1));
set(MyContext.guihandles.lbl_x,'String',num2str(MyContext.L(1)));

set(MyContext.guihandles.sld_y,'Value',MyContext.L(2));
set(MyContext.guihandles.lbl_y,'String',num2str(MyContext.L(2)));

set(MyContext.guihandles.sld_z,'Value',MyContext.L(3));
set(MyContext.guihandles.lbl_z,'String',num2str(MyContext.L(3)));

% --- Executes during object creation, after setting all properties.
function sld_j6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sld_j6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
global MyContext;
set(hObject,'Value',MyContext.J(6));
MyContext.L = 1000*fkine_irb120(MyContext.rob,(MyContext.J).*pi/180);

% --- Executes on slider movement.
function sld_x_Callback(hObject, eventdata, handles)
% hObject    handle to sld_x (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
global MyContext;

x_val = round(get(hObject,'Value'));
ang = ikine_irb120(MyContext.rob,[x_val,MyContext.L(2),MyContext.L(3)]./1000,MyContext.q);
if length(ang) == 6
    %TODO: Handle cases where message isn't sent successfully
    result = MyContext.networkModule.moveLinear(1, x_val);
    
    set(handles.lbl_x,'String',string(x_val));
    MyContext.L(1) = x_val;
    MyContext.J = ang*180/pi;

    set(MyContext.guihandles.sld_j1,'Value',MyContext.J(1));
    set(MyContext.guihandles.lbl_j1_val,'String',num2str(MyContext.J(1)));
    
    set(MyContext.guihandles.sld_j2,'Value',MyContext.J(2));
    set(MyContext.guihandles.lbl_j2_val,'String',num2str(MyContext.J(2)));
    
    set(MyContext.guihandles.sld_j3,'Value',MyContext.J(3));
    set(MyContext.guihandles.lbl_j3_val,'String',num2str(MyContext.J(3)));
    
    set(MyContext.guihandles.sld_j4,'Value',MyContext.J(4));
    set(MyContext.guihandles.lbl_j4_val,'String',num2str(MyContext.J(4)));
    
    set(MyContext.guihandles.sld_j5,'Value',MyContext.J(5));
    set(MyContext.guihandles.lbl_j5_val,'String',num2str(MyContext.J(5)));
    
    set(MyContext.guihandles.sld_j6,'Value',MyContext.J(6));
    set(MyContext.guihandles.lbl_j6_val,'String',num2str(MyContext.J(6)));
elseif isempty(ang)
    str = 'Robot destination can''t be reached. Try a new destination';
    set(MyContext.guihandles.lbl_Console,'string',str);
    set(MyContext.guihandles.sld_x,'Value',MyContext.L(1));
    set(MyContext.guihandles.lbl_x,'String',num2str(MyContext.L(1)));
end
% ang = ikine_irb120(MyContext.rob,MyContext.L/1000,MyContext.q)

% --- Executes during object creation, after setting all properties.
function sld_x_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sld_x (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function sld_y_Callback(hObject, eventdata, handles)
% hObject    handle to sld_y (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
global MyContext;

y_val = round(get(hObject,'Value'));
ang = ikine_irb120(MyContext.rob,[MyContext.L(1),y_val,MyContext.L(3)]./1000,MyContext.q);
if length(ang) == 6
    
    result = MyContext.networkModule.moveLinear(2, y_val);

    set(handles.lbl_y,'String',string(y_val));
    MyContext.L(2) = y_val;
    MyContext.J = ang*180/pi;
    set(MyContext.guihandles.sld_j1,'Value',MyContext.J(1));
    set(MyContext.guihandles.lbl_j1_val,'String',num2str(MyContext.J(1)));
    
    set(MyContext.guihandles.sld_j2,'Value',MyContext.J(2));
    set(MyContext.guihandles.lbl_j2_val,'String',num2str(MyContext.J(2)));
    
    set(MyContext.guihandles.sld_j3,'Value',MyContext.J(3));
    set(MyContext.guihandles.lbl_j3_val,'String',num2str(MyContext.J(3)));
    
    set(MyContext.guihandles.sld_j4,'Value',MyContext.J(4));
    set(MyContext.guihandles.lbl_j4_val,'String',num2str(MyContext.J(4)));
    
    set(MyContext.guihandles.sld_j5,'Value',MyContext.J(5));
    set(MyContext.guihandles.lbl_j5_val,'String',num2str(MyContext.J(5)));
    
    set(MyContext.guihandles.sld_j6,'Value',MyContext.J(6));
    set(MyContext.guihandles.lbl_j6_val,'String',num2str(MyContext.J(6)));
elseif isempty(ang)
    str = 'Robot destination can''t be reached. Try a new destination';
    set(MyContext.guihandles.lbl_Console,'string',str);
    set(MyContext.guihandles.sld_y,'Value',MyContext.L(2));
    set(MyContext.guihandles.lbl_y,'String',num2str(MyContext.L(2)));
end

% --- Executes during object creation, after setting all properties.
function sld_y_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sld_y (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function sld_z_Callback(hObject, eventdata, handles)
% hObject    handle to sld_z (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
global MyContext;

z_val = round(get(hObject,'Value'));
ang = ikine_irb120(MyContext.rob,[MyContext.L(1),MyContext.L(2),z_val]./1000,MyContext.q);
if length(ang) == 6
    
    result = MyContext.networkModule.moveLinear(3, z_val);

    set(handles.lbl_z,'String',string(z_val));
    MyContext.L(3) = z_val;
    MyContext.J = ang*180/pi;
    set(MyContext.guihandles.sld_j1,'Value',MyContext.J(1));
    set(MyContext.guihandles.lbl_j1_val,'String',num2str(MyContext.J(1)));
    
    set(MyContext.guihandles.sld_j2,'Value',MyContext.J(2));
    set(MyContext.guihandles.lbl_j2_val,'String',num2str(MyContext.J(2)));
    
    set(MyContext.guihandles.sld_j3,'Value',MyContext.J(3));
    set(MyContext.guihandles.lbl_j3_val,'String',num2str(MyContext.J(3)));
    
    set(MyContext.guihandles.sld_j4,'Value',MyContext.J(4));
    set(MyContext.guihandles.lbl_j4_val,'String',num2str(MyContext.J(4)));
    
    set(MyContext.guihandles.sld_j5,'Value',MyContext.J(5));
    set(MyContext.guihandles.lbl_j5_val,'String',num2str(MyContext.J(5)));
    
    set(MyContext.guihandles.sld_j6,'Value',MyContext.J(6));
    set(MyContext.guihandles.lbl_j6_val,'String',num2str(MyContext.J(6)));
elseif isempty(ang)
    str = 'Robot destination can''t be reached. Try a new destination';
    set(MyContext.guihandles.lbl_Console,'string',str);
    set(MyContext.guihandles.sld_z,'Value',MyContext.L(3));
    set(MyContext.guihandles.lbl_z,'String',num2str(MyContext.L(3)));
end

% --- Executes during object creation, after setting all properties.
function sld_z_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sld_z (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on selection change in pop_speed.
function pop_speed_Callback(hObject, eventdata, handles)
% hObject    handle to pop_speed (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns pop_speed contents as cell array
%        contents{get(hObject,'Value')} returns selected item from pop_speed


% --- Executes during object creation, after setting all properties.
function pop_speed_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pop_speed (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over pop_speed.
function pop_speed_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to pop_speed (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pbn_click2move.
function pbn_click2move_Callback(hObject, eventdata, handles)
% hObject    handle to pbn_click2move (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes during object creation, after setting all properties.
function axs_vidtab_CreateFcn(hObject, eventdata, handles)
% hObject    handle to axs_vidtab (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate axs_vidtab


% --- Executes on button press in pbn_procImg.
function pbn_procImg_Callback(hObject, eventdata, handles)
% hObject    handle to pbn_procImg (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global MyContext;

text = get(hObject,'String');
% toggle handling
if strcmp(text,'Start')
    MyContext.procImg(1) = 0;
%     if MyContext.offline == false
% %         axes(MyContext.guihandles.axs_vidtab);
%         cam = videoinput('winvideo',2,'RGB24_1600x1200');
%         preview(cam,MyContext.guihandles.axs_vidtab);
%     end
    
    % toggle text
    set(hObject,'String','Stop');
else
    MyContext.procImg(1) = 1;
%     if MyContext.offline == false
% %         axes(MyContext.guihandles.axs_vidtab);
%         cam = videoinput('winvideo',1,'RGB24_1600x1200');
%         preview(cam,MyContext.guihandles.axs_vidtab);
%     end
    % toggle text
    set(hObject,'String','Start');
end


% --- Executes during object creation, after setting all properties.
function axs_vidcon_CreateFcn(hObject, eventdata, handles)
% hObject    handle to axs_vidcon (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate axs_vidcon


% --- Executes on button press in rad_tab.
function rad_tab_Callback(hObject, eventdata, handles)
% hObject    handle to rad_tab (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of rad_tab
global MyContext;

set(hObject,'Value',1);
set(MyContext.guihandles.rad_con,'Value',0);
MyContext.procImg(2) = 1;


% --- Executes on button press in rad_con.
function rad_con_Callback(hObject, eventdata, handles)
% hObject    handle to rad_con (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of rad_con
global MyContext;
set(hObject,'Value',1);
set(MyContext.guihandles.rad_tab,'Value',0);
MyContext.procImg(2) = 2;
