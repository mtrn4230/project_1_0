MODULE RobIO
    PERS num Active_Flag;
    
    PROC AltVacRun(num state_in)
        
        ! DO10_1 = VacRun, DO10_2 = VacSol
        ! Check if already in desired state
        IF DO10_1 = state_in THEN       
            ! ========= ERROR
        ELSE
            ! Check if trying to turn on or off
            IF state_in = 1 THEN
                ! Set VacRun
                SetDO DO10_1, 1;         
            
            ! If trying to turn VacRun off
            ELSE
                ! If trying to turn off, Check if VacSol is set
                IF DO10_2 = 1 THEN
                    ! ====== ERROR - VacSol = 1
                    ! Reset VacSol first
                    SetDO DO10_2, 0;
                    SetDO DO10_1, 0;
                ELSE  
                    ! Reset VacRun
                    SetDO DO10_1, 0;
                ENDIF
                
            ENDIF
            
        ENDIF
        
    ENDPROC
    
    PROC AltVacSol(num state_in)
        
        ! DO10_1 = VacRun, DO10_2 = VacSol
        ! Check if already in desired state
        IF DO10_2 = state_in THEN       
            ! ========= ERROR
        ELSE
            ! Check if trying to turn on or off
            IF state_in = 1 THEN
                
                ! If trying to turn on, Check if VacRun is set
                IF DO10_1 = 1 THEN
                    ! Set VacSol on
                    SetDO DO10_2, 1;
                ELSE  
                    ! ======ERROR - VacRun = 0
                    SetDO DO10_2, 0;
                ENDIF
            
            ! If trying to turn VacSol off
            ELSE
                SetDO DO10_2, 0;   
            ENDIF
            
        ENDIF
        
    ENDPROC
    
    PROC AltConRun(num state_in)
        
        ! DI10_1 = ConStat, DO10_3 = ConRun
        ! ConStat will only be equal to 1 if the conveyor is on and ready to run.
        ! If it is ready to run, we will run it, if not, we will set it off so that we can fix it.
        IF DI10_1 = 1 THEN
            
            ! Check if already in desired state
            IF DO10_3 = state_in THEN
                ! ========= ERROR
            ELSE
                ! Set ConRun on/off.
                SetDO DO10_3, state_in;
            ENDIF
            
        ELSE
            SetDO DO10_3, 0;
            ! ========== ERROR
            
        ENDIF
        
    ENDPROC
    
    PROC AltConDir(num state_in)
        
        ! DI10_1 = ConStat, DO10_3 = ConRun, DO10_4 = ConDir(0 = South, 1 = North)
        
        ! ConStat will only be equal to 1 if the conveyor is on and ready to run.
        ! If it is ready to run, we will run it, if not, we will set it off so that we can fix it.
        IF DI10_1 = 1 THEN 
            
            ! Check that Conveyor is not currently running
            IF DO10_3 = 0 THEN
                
                ! Check if already in desired state
                IF DO10_4 = state_in THEN
                    ! ========= ERROR
                ELSE
                    ! Set ConDir
                    SetDO DO10_4, state_in;
                ENDIF
            
            ELSE
                ! ========= ERROR - Conveyor Running
                ! Don't Change Direction
            ENDIF
            
        ELSE
            SetDO DO10_4, 0;
            ! ========== ERROR
            
        ENDIF
        
    ENDPROC
ENDMODULE