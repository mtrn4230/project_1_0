MODULE networkModule    

    ! specify structs (records)
    RECORD Header
         byte status;
         byte cmdNum;
         byte subCmd;
         byte seqNum;
         byte terminating;
         byte dataLength;
         
    ENDRECORD
    
    RECORD MoveData
        num moveType;  ! 0-Linear, 1-Joint. Only the matching position or joint record should be filled. (Can possibly add 3-OverTable for 'move to pos(x,y) in the fixed end effector orientation, z=12' , zheight and orientation can be fixed in moveModule)
        num axisMove; ! 1 - x, 2 - y, 3 - z;
        num jointMove; ! 0 = alljoints, 1 - joint 1 ... 6 - joint 6
        pos linearTarget;
        robjoint jointTarget;
        num speedData;          !Not 100% sure what type of speed (tool point, joint vel) this matches yet, but one number is fine, as the spec only requires one type of speed to change. It doesnt spec which. 
        bool isLocked;
        bool isReady;
    ENDRECORD
    
    RECORD IOStatus !FOR FEEDBACK
        !Robot Outputs
        bool conRun;
        bool conDir;
        bool vacPump;
        bool vacSol;  
        !Robot Inputs
        bool conStat;
        !bool eStop;    !TO DO
    ENDRECORD
    
    RECORD DesiredIOStatus  !Controlled by networkModule
        bool conRun;
        bool conDir;
        bool vacPump;
        bool vacSol;
    ENDRECORD
        
        
    ! The socket connected to the client.
    VAR socketdev client_socket;
    VAR rawbytes received_packet;
    VAR rawbytes  outgoing_packet;
    CONST num headerSize := 2;
    ! The host and port that we will be listening for a connection on.
    ! CONST string host := "192.168.125.4"; ! public
    CONST string host := "127.0.0.1"; ! local
    CONST num port := 1025;
    
    PERS bool sysRunning; ! := TRUE;
    PERS bool moveModuleRunning; ! := FALSE;
    !PERS bool IOModuleRunning := FALSE;
    
    !PERS bool restartFlag := FALSE;
    PERS MoveData newMove;! := [ 0, 0, 0, [0,0,0], [0,0,0,0,0,0], 0, FALSE, TRUE ]; ! move type, axis move, joint move, axes, angles, speed, isLocked, isReady
    !PERS IOStatus feedbackIO := [ FALSE, FALSE, FALSE, FALSE, FALSE ];
    !PERS DesiredIOStatus controlIO := [ FALSE, FALSE, FALSE, FALSE ];

    ! open socket and connect to matlab
    PROC Main()
        ListenForAndAcceptConnection;

        !Network Module is ready -- Flag system start
        SysRunning := TRUE;
        WaitUntil (moveModuleRunning);! AND IOModuleRunning);
        
        MainServer;
        ! More stuff to add here, spinning up the other systems
        
    ENDPROC
        
    
    PROC MainServer()
        VAR Header headerIn;
        VAR string testString;
        VAR byte serializedHeader{2};
        !sysRunning := TRUE;     
        ! Receive a string from the client.
        WHILE (TRUE) DO
            SocketReceive client_socket, \RawData:=received_packet \Time:=600;
            ! deserialize the header
            
            UnpackRawBytes received_packet, 1, serializedHeader{1} \ASCII:=1;
            UnpackRawBytes received_packet, 2, serializedHeader{2} \ASCII:=2;
            headerIn := DeserializeHeader(serializedHeader);
            
            ExecuteCmd(headerIn);
            
            
        ENDWHILE
        
        ! Some stuff needs to happen here in regards to shut down
        !waitUntil ( (NOT moveModuleRunning) );
        CloseConnection;
		
    ENDPROC
    
    PROC ExecuteCmd(Header headerIn)
        IF headerIn.cmdNum = 0 THEN
            processDebugCmd(headerIn);
        ELSEIF headerIn.cmdNum = 1 THEN
            processStartupCmd(headerIn);
        ELSEIF headerIn.cmdNum = 2 THEN
            processMoveCmd(headerIn);
        ELSEIF headerIn.cmdNum = 3 THEN
            processReadCmd(headerIn);
        ELSEIF headerIn.cmdNum = 4 THEN
            processSetDigitalCmd(headerIn);
        ELSEIF headerIn.cmdNum = 5 THEN
            processReadDigitalCmd(headerIn);
        ENDIF
        
    ENDPROC
    
    PROC processDebugCmd(Header headerIn)
        
    ENDPROC
    
    PROC processStartupCmd(Header headerIn)
        
    ENDPROC
    
    PROC processMoveCmd(Header headerIn)
        VAR num jointNum;
        VAR num axisNum;
        VAR num axisValue;
        VAR num axesValues{3};
        VAR num jointAngle;
        VAR num jointAngles{6};
        
        IF headerIn.subCmd = 0 THEN ! moveJoint
        
            UnpackRawBytes received_packet, headerSize+1,  jointNum \IntX:=1;
            UnpackRawBytes received_packet, headerSize+2,  jointAngle \Float4;
            ErrWrite "Data = ", NumToStr(jointNum, 0), \RL2:=NumToStr(jointAngle, 3);
            
            WaitUntil NOT newMove.isLocked;
            newMove.isLocked := TRUE;
            newMove.moveType := 1; ! joint move
            newMove.axisMove := 0;
            newMove.jointMove := jointNum;
            newMove.linearTarget := [0, 0, 0];
            jointAngles := [0,0,0,0,0,0];
            jointAngles{jointNum} := jointAngle;
            newMove.jointTarget.rax_1 := jointAngles{1};
            newMove.jointTarget.rax_2 := jointAngles{2};
            newMove.jointTarget.rax_3 := jointAngles{3};
            newMove.jointTarget.rax_4 := jointAngles{4};
            newMove.jointTarget.rax_5 := jointAngles{5};
            newMove.jointTarget.rax_6 := jointAngles{6};
            newMove.speedData := 0;
            newMove.isLocked := FALSE;

            RETURN;
        ELSEIF headerIn.subCmd = 1 THEN ! moveAllJoints
        
            WaitUntil NOT newMove.isLocked;
            newMove.isLocked := TRUE;
            newMove.moveType := 0;
            newMove.axisMove := 0;
            newMove.jointMove := 0;
            newMove.linearTarget := [0, 0, 0];
            newMove.speedData := 0;
            FOR i FROM 1 TO 6 DO 
                UnpackRawBytes received_packet, headerSize+((i-1)*4)+1, jointAngles{i} \Float4;
            ENDFOR
            newMove.jointTarget.rax_1 := jointAngles{1};
            newMove.jointTarget.rax_2 := jointAngles{2};
            newMove.jointTarget.rax_3 := jointAngles{3};
            newMove.jointTarget.rax_4 := jointAngles{4};
            newMove.jointTarget.rax_5 := jointAngles{5};
            newMove.jointTarget.rax_6 := jointAngles{6};
            
            ErrWrite "Data = ", NumToStr(jointAngles{1}, 3) + " " + NumToStr(jointAngles{2}, 3) + " "
                     + NumToStr(jointAngles{3}, 3) + " " + NumToStr(jointAngles{4}, 3) + " " +
                     NumToStr(jointAngles{5}, 3) + " " + NumToStr(jointAngles{6}, 3);
        
            newMove.isLocked := TRUE;
            RETURN;
        ELSEIF headerIn.subCmd = 2 THEN ! Move linear
            UnpackRawBytes received_packet, headerSize+1, axisNum \IntX:=1;
            UnpackRawBytes received_packet, headerSize+2, axisValue \Float4;
            
            WaitUntil NOT newMove.isLocked;
            newMove.moveType := 1;
            newMove.axisMove := axisNum;
            newMove.jointMove := 0;
            newMove.jointTarget := [0, 0, 0, 0, 0, 0];
            newMove.linearTarget := [0, 0, 0];
            newMove.speedData := 0;
            axesValues := [0,0,0];
            axesValues{axisNum} := axisValue;
            newMove.linearTarget.x := axesValues{1};
            newMove.linearTarget.y := axesValues{2};
            newMove.linearTarget.z := axesValues{3};
            newMove.isLocked := TRUE;
            RETURN;
        ENDIF
        
        
            
    ENDPROC
    
    PROC processReadCmd(Header headerIn)
        
    ENDPROC
    
    PROC processSetDigitalCmd(Header headerIn)
        VAR DesiredIOStatus status;
        VAR Byte settings;
        IF headerIn.subCmd = 0 THEN
            UnpackRawBytes received_packet, headerSize+1, settings \ASCII:=1;
            IF BitRSh(settings, 7) = 1 THEN
                status.conRun := TRUE;
            ELSE
                status.conRun := FALSE;
            ENDIF
            
            IF BitAnd(BitRSh(settings, 6), 1) = 1 THEN
                status.conDir := TRUE;
            ELSE
                status.conDir := FALSE;
            ENDIF
            
            IF BitAnd(BitRSh(settings, 5), 1) = 1 THEN
                status.vacPump := TRUE;
            ELSE
                status.vacPump := FALSE;
            ENDIF
            
            IF BitAnd(BitRSh(settings, 4), 1) = 1 THEN
                status.vacSol := TRUE;
            ELSE
                status.vacSol := FALSE;
            ENDIF
        ENDIF
    ENDPROC
    
    PROC processReadDigitalCmd(Header headerIn)
        
    ENDPROC
    
    PROC ListenForAndAcceptConnection()
        
        ! Create the socket to listen for a connection on.
        VAR socketdev welcome_socket;
        SocketCreate welcome_socket;
        
        ! Bind the socket to the host and port.
        SocketBind welcome_socket, host, port;
        
        ! Listen on the welcome socket.
        SocketListen welcome_socket;
        
        ! Accept a connection on the host and port.
        SocketAccept welcome_socket, client_socket;
        
        ! Close the welcome socket, as it is no longer needed.
        SocketClose welcome_socket;
        
    ENDPROC
        
    
    ! Close the connection to the client.
    PROC CloseConnection()
        SocketClose client_socket;
    ENDPROC
		
    LOCAL FUNC Header DeserializeHeader(byte serializedHeader{*})
        
        VAR Header headerIn;
        CONST num headerSizes{6} := [2, 3, 2, 1, 2, 6];
        VAR num headerSize := headerSizes{1} + headerSizes{2} + headerSizes{3} + headerSizes{4} + headerSizes{5} + headerSizes{6};
        VAR num packetPtr := 1;
        VAR num headerPtr := 1;
        VAR byte inByte;
        VAR string raw_str;
        VAR num rawNum;
        VAR num bytePtr;
        VAR num bitShift;
        
        WHILE (packetPtr <= headerSize) DO
            ! read in packet
            bytePtr := trunc(packetPtr/9) + 1;

            inByte := serializedHeader{bytePtr};
            bitShift := packetPtr - 8*(bytePtr-1);
            IF bitShift <> 1 THEN
                inByte := BitLSh(inByte, bitShift - 1);
            ENDIF
            IF headerSizes{headerPtr} <> 8 THEN
                inByte := BitRSh(inByte, 8-headerSizes{headerPtr});
            ENDIF
            IF headerPtr = 1 THEN
                headerIn.status := inByte;
            ELSEIF headerPtr = 2 THEN
                headerIn.cmdNum := inByte;
            ELSEIF headerPtr = 3 THEN
                headerIn.subCmd := inByte;
            ELSEIF headerPtr = 4 THEN
                headerIn.terminating := inByte;
            ELSEIF headerPtr = 5 THEN
                headerIn.seqNum := inByte;
            ELSEIF headerPtr = 6 THEN
                headerIn.dataLength := inByte;
            ENDIF
            
            packetPtr := packetPtr + headerSizes{headerPtr};
            headerPtr := headerPtr + 1;
            
        ENDWHILE
        
        ErrWrite "header", "status: " + NumToStr(headerIn.status, 0)
                 + " cmdNum: " + NumToStr(headerIn.cmdNum, 0) + " subCmd: " 
                 + NumToStr(headerIn.subCmd, 0) + " terminate: " + NumToStr(headerIn.terminating, 0) 
                 + " seqNum: " + NumToStr(headerIn.seqNum, 0) + " DataLength: " + NumToStr(headerIn.dataLength, 0);
        
        RETURN headerIn;
    ENDFUNC
    
ENDMODULE