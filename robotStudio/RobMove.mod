MODULE RobMove
    ! Persistents taken in from packet
    PERS num Active_Flag;
    PERS num Move_Flag;
    PERS robtarget target_p;
    PERS jointtarget target_j;
    PERS bool isMoving;
    
    CONST num T1_x := pTableT1.trans.x;
    CONST num T1_y := pTableT1.trans.y;                  
    CONST num T2_x := pTableT2.trans.x;
    CONST num T2_y := pTableT2.trans.y;
    CONST num T3_x := pTableT3.trans.x;
    CONST num T3_y := pTableT3.trans.y;
    CONST num T4_x := pTableT4.trans.x;
    CONST num T4_y := pTableT4.trans.y;
                      
    PROC MoveMain()
        VAR robtarget target_tmp;
        VAR jointtarget target_j_tmp;       
        VAR speeddata speed_tmp;
        VAR robtarget p1;
        
        WHILE Active_Flag = 1 DO
            IF Move_Flag = 1 THEN ! JOINT MODE/HIGH SPEED
                
                target_tmp := target_p; 
                isMoving := TRUE;
                MoveJ target_tmp, v100, fine, tSCup;
                isMoving := FALSE;
                
            ELSEIF Move_Flag = 2 THEN ! JOINT MODE/LOW SPEED
                
                target_tmp := target_p; 
                isMoving := TRUE;
                MoveJ target_tmp, v50, fine, tSCup;
                isMoving := FALSE;
            
            ELSEIF Move_Flag = 3 THEN ! LINEAR MODE/HIGH SPEED            
            
                target_tmp := target_p;
                isMoving := TRUE;
                MoveL target_tmp, v100, fine, tSCup;
                isMoving := FALSE;
 
            ELSEIF Move_Flag = 4 THEN ! LINEAR MODE/LOW SPEED            
            
                target_tmp := target_p;
                isMoving := TRUE;
                MoveL target_tmp, v50, fine, tSCup;
                isMoving := FALSE;
              
            ELSEIF Move_Flag = 5 THEN ! Move individual Joint/HIGH SPEED
            
                target_j_tmp := target_j;  
                isMoving := TRUE;
                MoveAbsJ target_j_tmp, v100, fine, tSCup;
                isMoving := FALSE;
                
            ELSEIF Move_Flag = 6 THEN ! Move individual Joint/LOW SPEED
            
                target_j_tmp := target_j;  
                isMoving := TRUE;
                MoveAbsJ target_j_tmp, v50, fine, tSCup;
                isMoving := FALSE;
            ENDIF    
        ENDWHILE
    ENDPROC
    
    FUNC robtarget getCurPos()
        VAR robtarget CurPos;
        CurPos := CRobT(\Tool:=tSCup);
        RETURN CurPos;
    ENDFUNC

!    FUNC bool isReachable(robtarget targ)
!        VAR bool reachable;
!        VAR num targ_x := targ.trans.x;
!        VAR num targ_y := targ.trans.y;
!        VAR num targ_z := targ.trans.z;
!        VAR num r;
!        d1 := ((targ_x - T1_x)*(targ_x - T1_x))/(T2_y*T2_y);
!        IF THEN
!        ELSE THEN
!        ENDIF
        
!        RETURN reachable;
!    ENDFUNC
!    ((((regionProps.Centroids(i,1)-O(1))^2/r_l^2)+((regionProps.Centroids(i,2)-O(2))^2/r_f^2)<=1) ...
!                || (((regionProps.Centroids(i,1)-O(1))^2/r_r^2)+((regionProps.Centroids(i,2)-O(2))^2/r_f^2)<=1))
    
ENDMODULE