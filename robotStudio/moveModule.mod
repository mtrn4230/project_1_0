MODULE moveMove
    
    RECORD MoveData
        num moveType;  ! 0-Linear, 1-Joint. Only the matching position or joint record should be filled. (Can possibly add 3-OverTable for 'move to pos(x,y) in the fixed end effector orientation, z=12' , zheight and orientation can be fixed in moveModule)
        num axisMove; ! 1 - x, 2 - y, 3 - z;
        num jointMove; ! 0 = alljoints, 1 - joint 1 ... 6 - joint 6
        pos linearTarget;
        robjoint jointTarget;
        num speedData;          !Not 100% sure what type of speed (tool point, joint vel) this matches yet, but one number is fine, as the spec only requires one type of speed to change. It doesnt spec which. 
        bool isLocked;
        bool isReady;
    ENDRECORD

!    CONST num T1_x := pTableT1.trans.x;
!    CONST num T1_y := pTableT1.trans.y;                  
!    CONST num T2_x := pTableT2.trans.x;
!    CONST num T2_y := pTableT2.trans.y;
!    CONST num T3_x := pTableT3.trans.x;
!    CONST num T3_y := pTableT3.trans.y;
!    CONST num T4_x := pTableT4.trans.x;
!    CONST num T4_y := pTableT4.trans.y;
        
    !VAR intnum pauseSignal;
    !PERS bool restartFlag;
    
    PERS MoveData newMove := [ 0, 0, 0, [0,0,0], [0,0,0,0,0,0], 0, TRUE, TRUE ]; ! move type, axis move, joint move, axes, angles, speed, isLocked, isReady
    PERS bool sysRunning := TRUE;
    PERS bool moveModuleRunning := FALSE;                  
    
    PROC MoveMain()
        
        
        VAR MoveData curMove;
        
        
        VAR robtarget target;
        VAR jointtarget  target_angular;
        VAR speeddata speed_tmp;
        VAR robtarget p1;
        
        
        !CONNECT pauseSignal WITH pauseProcess;
        !ISignalDO DO1, 1, pauseSignal;
        
        speed_tmp := [50,50,50,50]; 
        moveModuleRunning := TRUE;
        WaitUntil sysRunning;
        
        WHILE moveModuleRunning AND sysRunning DO
            WaitUntil NOT newMove.isLocked AND newMove.isReady;
            newMove.isLocked := TRUE;
            curMove := newMove;
            newMove.isReady := FALSE;
            newMove.isLocked := FALSE;
            target := CRobT(\Tool:=tSCup);
            IF curMove.moveType = 0 THEN
                ! 'MoveL' executes a joint motion towards a robtarget. This is used to move the robot quickly from one point to another when that 
                !   movement does not need to be in a straight line.
                ! 'target_tmp' is a robtarget defined in system module. The exact location of this on the table has been provided to you.
                ! 'speed_tmp' is a speeddata variable, and defines how fast the robot should move. The numbers is the speed in mm/sec
                ! 'fine' is a zonedata variable, and defines how close the robot should move to a point before executing its next command. 
                !   'fine' means very close, other values such as 'z10' or 'z50', will move within 10mm and 50mm respectively before executing the next command.
                ! 'tSCup' is a tooldata variable. This has been defined in a system module, and represents the tip of the suction cup, telling the robot that we
                !   want to move this point to the specified robtarget. 
                !isMoving := TRUE;
                IF curMove.axisMove = 0 THEN
                    target.trans := curMove.linearTarget;
                ELSEIF curMove.axisMove = 1 THEN
                    target.trans.x := curMove.linearTarget.x;
                ELSEIF curMove.axisMove = 2 THEN
                    target.trans.y := curMove.linearTarget.y;
                ELSEIF curMove.axisMove = 3 THEN
                    target.trans.z := curMove.linearTarget.z;
                ENDIF
                MoveL target, speed_tmp, fine, tSCup;
                
            ELSEIF curMove.moveType = 1 THEN ! LINEAR MODE
                
                target_angular := CalcJointT(target, tSCup);
                IF curMove.jointMove = 0 THEN
                    target_angular.robax := curMove.jointTarget;
                ELSEIF curMove.jointMove = 1 THEN
                    target_angular.robax.rax_1 := curMove.jointTarget.rax_1;
                ELSEIF curMove.jointMove = 2 THEN
                    target_angular.robax.rax_2 := curMove.jointTarget.rax_2;
                ELSEIF curMove.jointMove = 3 THEN
                    target_angular.robax.rax_3 := curMove.jointTarget.rax_3;
                ELSEIF curMove.jointMove = 4 THEN
                    target_angular.robax.rax_4 := curMove.jointTarget.rax_4;
                ELSEIF curMove.jointMove = 5 THEN
                    target_angular.robax.rax_5 := curMove.jointTarget.rax_5;
                ELSEIF curMove.jointMove = 6 THEN
                    target_angular.robax.rax_6 := curMove.jointTarget.rax_6;
                ENDIF
                !speed_tmp := curMove.speedData;
                
                ! 'MoveL' executes a linear motion towards a robtarget. This is used to move the robot quickly from one point to another when that 
                !   movement does not need to be in a straight line.
                ! 'target_tmp' is a robtarget defined in system module. The exact location of this on the table has been provided to you.
                ! 'speed_tmp' is a speeddata variable, and defines how fast the robot should move. The numbers is the speed in mm/sec
                ! 'fine' is a zonedata variable, and defines how close the robot should move to a point before executing its next command. 
                !   'fine' means very close, other values such as 'z10' or 'z50', will move within 10mm and 50mm respectively before executing the next command.
                ! 'tSCup' is a tooldata variable. This has been defined in a system module, and represents the tip of the suction cup, telling the robot that we
                !   want to move this point to the specified robtarget.
                !isMoving := TRUE;
                
                !moveAbsJ 
                MoveAbsJ target_angular, speed_tmp, fine, tSCup;
                !isMoving := FALSE;
            !ELSEIF Move_Flag = 3 THEN ! Move individual Joint
                   
            ENDIF    
        ENDWHILE
    ENDPROC
    
    !TRAP pauseProcess
    !    !BE PAUSED
    !    WaitUntil restartFlag;  !Dunno if this will be actual method
    !ENDTRAP
    
    FUNC robtarget getCurPos()
        VAR robtarget CurPos;
        CurPos := CRobT(\Tool:=tSCup);
        RETURN CurPos;
    ENDFUNC

!    FUNC bool isReachable(robtarget targ)
!        VAR bool reachable;
!        VAR num targ_x := targ.trans.x;
!        VAR num targ_y := targ.trans.y;
!        VAR num targ_z := targ.trans.z;
!        VAR num r;
!        d1 := ((targ_x - T1_x)*(targ_x - T1_x))/(T2_y*T2_y);
!        IF THEN
!        ELSE THEN
!        ENDIF
        
!        RETURN reachable;
!    ENDFUNC
!    ((((regionProps.Centroids(i,1)-O(1))^2/r_l^2)+((regionProps.Centroids(i,2)-O(2))^2/r_f^2)<=1) ...
!                || (((regionProps.Centroids(i,1)-O(1))^2/r_r^2)+((regionProps.Centroids(i,2)-O(2))^2/r_f^2)<=1))
    
ENDMODULE