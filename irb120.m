classdef irb120 < handle
    properties
        q;
        irb;
        
    end
    
    methods
        %% constructor
        function obj = irb120(q)
            
            startup_rvc
%             global MyContext;
            %L(i) = Link([theta d a alpha]) where d,a in metres and theta,alpha in radians
            L(1) = Link([0      0.290    0       pi/2]);
            L(2) = Link([0      0        0.270   0]);
            L(3) = Link([0      0        0.07    -pi/2]);
            L(4) = Link([0      0.302    0       pi/2]);
            L(5) = Link([0      0        0       pi/2]);
            L(6) = Link([0      0.137    0       0]);
            
            %L(i).offset = offset in radians
            L(1).offset = pi;
            L(2).offset = pi/2;
            L(3).offset = 0;
            L(4).offset = 0;
            L(5).offset = pi;
            L(6).offset = 0;
            
            obj.irb = SerialLink(L, 'name', 'irb120');
            T = obj.irb.fkine(q);
        
        end
        %% forward kinematics for irb120
        function pose = fkine_irb120(obj,ang)
            
            if size(ang) ~= 6
                disp('q must be a vector of 6 angles');
                return
            end
            obj.q = ang;
            
            T = obj.irb.fkine(obj.q);
            pose = T.t;
        end
        
        %% inverse kinematics for irb120
        function q = ikine_irb120(obj,pose,quat)
            if size(pose) ~= 3
                disp('pose must be a vector of 3 points in xyz');
                return
            end
            if size(quat) ~= 4
                disp('quat must be a vector of 4 quaternions');
                return
            end
            
            quaternion = UnitQuaternion(quat);
            T = r2t(quaternion.R);
            T(1:3,4) = [pose(1);pose(2);pose(3)];
            q = obj.irb.ikine(T);
            
        end
        
    end
    
end